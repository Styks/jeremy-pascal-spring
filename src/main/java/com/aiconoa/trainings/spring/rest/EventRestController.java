package com.aiconoa.trainings.spring.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiconoa.trainings.spring.domain.entity.Event;
import com.aiconoa.trainings.spring.domain.repository.EventRepository;

@RestController
@RequestMapping(path="/api")
public class EventRestController {
	
	@Autowired
	private EventRepository eventService;
	
	@RequestMapping(path="/hello", method=RequestMethod.GET)
	public String hello(){
		return "hello Spring";
	}

	@RequestMapping(path="/events", method=RequestMethod.GET)
	public List<Event> getAllEvents() {
		return eventService.findAll();
	}
}
