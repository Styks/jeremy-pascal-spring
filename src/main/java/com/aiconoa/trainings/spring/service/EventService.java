package com.aiconoa.trainings.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiconoa.trainings.spring.domain.entity.Author;
import com.aiconoa.trainings.spring.domain.entity.Event;
import com.aiconoa.trainings.spring.domain.entity.Subscriber;
import com.aiconoa.trainings.spring.domain.entity.User;
import com.aiconoa.trainings.spring.domain.repository.AuthorRepository;
import com.aiconoa.trainings.spring.domain.repository.EventRepository;
import com.aiconoa.trainings.spring.domain.repository.SubscriberRepository;
import com.aiconoa.trainings.spring.domain.repository.UserRepository;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	@Autowired
	private AuthorRepository authorRepository;
	@Autowired
	private SubscriberRepository subscriberRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private MailService mailService;

	public List<Event> findAllEvents() {
		return eventRepository.findAll();
	}

	public Event save(Event event) {
		return eventRepository.save(event);
	}

	public Author save(Author author) {
		return authorRepository.save(author);
	}

	public boolean checkIsUniqueUserByUsername(String Username) {
		return authorRepository.countByUsername(Username) == 1L;
	}

	public Author findAuthorByUserName(String Username) {
		return authorRepository.findByUsername(Username);
	}

	public Event findEvent(long idEvent) {
		return eventRepository.findOne(idEvent);
	}

	public List<Subscriber> findByEvent(Event event) {
		return subscriberRepository.findByEvent(event);
	}

	public User findUserByMail(String email) {
		return userRepository.findByEmail(email);
	}

	public void addSubscribersToEvent(Event event, String emailList) {
		for (String email : emailList.split(";")) {
			User user = new User();
			if (findUserByMail(email) == null) {
				user.setEmail(email);
				user = userRepository.save(user);
			} else {
				user = findUserByMail(email);
			}
			Subscriber subscriber = new Subscriber();
			if (subscriberRepository.findByEventAndUser(event, user) == null) {
				subscriber.setHashcode(Hasher.sha1ToHex(user.getEmail() + "#" + event.getIdevent()));
				subscriber.setEvent(event);
				subscriber.setUser(user);
				subscriber = subscriberRepository.save(subscriber);
			} else {
				subscriber = subscriberRepository.findByEventAndUser(event, user);
			}
			mailService.sendEmail(subscriber);
			// (new MyEventMail(email, event.getTitle(), hash));
		}
	}

	public int countSubscriberByEventAndIsPresent(Event event, int accept) {
				return subscriberRepository.countSubscriberByEventAndIsPresent(event,accept);
	}

	public int countSubscriberByEventAndMailStatus(Event event, int mailStatus) {
		return subscriberRepository.countSubscriberByEventAndMailStatus(event,mailStatus);
	}

	public int countSubscriberByEventAndIsPresentAndMailStatus(Event event, int present,  int mailStatus) {
		return subscriberRepository.countSubscriberByEventAndIsPresentAndMailStatus(event,present,mailStatus);
	}

	public List<Event> findByAuthor(Author author) {
		
		return eventRepository.findByAuthor(author);
	}

	public boolean checkIfEventCorrespondToAuthor(long idEvent, Author author) {
		  if (eventRepository.countByIdevent(idEvent)!=1L){
	            return false;
	        }
	        if (author.getIdauthor() != eventRepository.findOne(idEvent).getAuthor().getIdauthor()) {
	            return false;
	        }
	        return true;
	}


}
