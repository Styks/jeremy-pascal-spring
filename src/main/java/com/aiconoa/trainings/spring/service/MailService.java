package com.aiconoa.trainings.spring.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.aiconoa.trainings.spring.domain.entity.Subscriber;

@Service
public class MailService {
	@Autowired
	private JavaMailSender javaMailSender;

	public void sendEmail(Subscriber subscriber) {

		MimeMessage mail = javaMailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(subscriber.getUser().getEmail());
			helper.setSubject(
					"Bonjour à toi ! Tu as été invité à l'évent endiablé : " + subscriber.getEvent().getTitle());
			helper.setText("", "<h1 style=\"color:red;text-align: center;\">" + subscriber.getEvent().getTitle()
					+ "</h1>" + "<p style=\"text-align: center;\">" + subscriber.getEvent().getDescription() + "</p>"
					+ "<p style=\"text-align: center;\">Si tu veux participer à cet événement, suis les instructions du lien suivant :</p>"
					+ "<p style=\"text-align: center;\"><a href=\"http://localhost:8080/web/confirmation.html?token="
					+ subscriber.getHashcode() + "\">http://localhost:8080/web/confirmation.html?token="
					+ subscriber.getHashcode() + "</a></p>"
					+ "<img alt=\"\" src=\"http://localhost:8080/event/TrackingMail?token=" + subscriber.getHashcode()
					+ "\">");
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
		}
		javaMailSender.send(mail);
		// return helper;
	}
}
