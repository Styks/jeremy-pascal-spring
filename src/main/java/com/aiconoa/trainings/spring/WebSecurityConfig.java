package com.aiconoa.trainings.spring;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "home", "/css/*").permitAll().antMatchers("/protege").hasRole("ADMIN")
				.antMatchers("/anonymous").anonymous().anyRequest().authenticated()

				.and().formLogin().loginPage("/web/login").permitAll()

				.defaultSuccessUrl("/web/eventsList").and().logout().permitAll().logoutUrl("/web/logout");

		http.exceptionHandling().accessDeniedPage("/error/403");
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
				.usersByUsernameQuery("select username,password,true From Author Where username=?")
				.authoritiesByUsernameQuery("select username,authority From Author Where username=?")
				.dataSource(dataSource).passwordEncoder(new BCryptPasswordEncoder(12));

	}

}
