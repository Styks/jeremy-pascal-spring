package com.aiconoa.trainings.spring.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aiconoa.trainings.spring.domain.entity.Author;
import com.aiconoa.trainings.spring.domain.entity.Event;
import com.aiconoa.trainings.spring.domain.entity.EventStatData;
import com.aiconoa.trainings.spring.domain.entity.Subscriber;
import com.aiconoa.trainings.spring.service.EventService;

@Controller
@RequestMapping(path = "/web")
// @SessionAttributes("author")
public class EventController {
	private static final int ACCEPT = 1;
	private static final int DECLINE = 2;
	private static final int PRESENT = 1;
	private static final int NOTPRESENT = 2;

	@Autowired
	private EventService eventService;
	@Autowired
	ServletContext servletContext;

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/web/login";
	}

	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("author", new Author());
		return "login";
	}

	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public String checkAuthor() {
		return "eventsList";
	}

	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public String register(Model model) {
		model.addAttribute("author", new Author());
		return "register";
	}

	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public String saveAuthor(@ModelAttribute("author") Author author, RedirectAttributes attributes) {
		if (eventService.checkIsUniqueUserByUsername(author.getUsername())) {
			return "register";
		}
		if (author.getPassword().compareTo(author.getPassword2()) != 0) {
			return "register";
		}
		author.setPassword(BCrypt.hashpw(author.getPassword(), BCrypt.gensalt(12)));
		author = eventService.save(author);
		attributes.addFlashAttribute("message", "Vous êtes enregistré avec succès!");
		return "redirect:eventCreation";
	}

	@RequestMapping(path = "/eventsList", method = RequestMethod.GET)
	public String getAllEvents(Model model, Principal principal) {
		Author author = new Author();
		author = eventService.findAuthorByUserName(principal.getName());
		List<Event> events = eventService.findByAuthor(author);
		model.addAttribute("events", events);
		model.addAttribute("author", author);
		return "eventsList";
	}

	@RequestMapping(path = "/eventsList", method = RequestMethod.POST)
	public String newEvent(RedirectAttributes attributes) {
		return "redirect:eventCreation";
	}

	@RequestMapping(path = "/eventCreation", method = RequestMethod.GET)
	public String getNewEvent(Model model) {
		model.addAttribute("event", new Event());
		return "eventCreation";
	}

	@RequestMapping(value = "/eventCreation", method = RequestMethod.POST)
	public String saveEvent(Principal principal, @ModelAttribute Event event, @RequestParam("file") MultipartFile file,
			RedirectAttributes attributes) {
		Author author = eventService.findAuthorByUserName(principal.getName());
		event.setAuthor(author);
		event.setFileName("");
		if (!file.isEmpty()) {
			String name = file.getOriginalFilename();
			event.setFileName(name);
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("/uploads", name)));
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		eventService.save(event);
		attributes.addFlashAttribute("message", "Evenement ajouté avec succès!");
		return "redirect:eventLink?id=" + event.getIdevent();
	}

	@RequestMapping(path = "/eventEdition", method = RequestMethod.GET)
	public String getEvent(Principal principal, @RequestParam("id") long idEvent, Model model) {
		Author author = eventService.findAuthorByUserName(principal.getName());
		if (!eventService.checkIfEventCorrespondToAuthor(idEvent, author)) {
			throw new AccessDeniedException("non authorisé");
		}

		Event event = eventService.findEvent(idEvent);
		model.addAttribute("event", event);
		return "eventEdition";
	}

	@RequestMapping(value = "/eventEdition", method = RequestMethod.POST)
	public String updateEvent(Principal principal, @ModelAttribute Event event,
			@RequestParam("file") MultipartFile file, RedirectAttributes attributes) {
		Author author = eventService.findAuthorByUserName(principal.getName());
		if (!eventService.checkIfEventCorrespondToAuthor(event.getIdevent(), author)) {
			throw new AccessDeniedException("non authorisé");
		}
		event.setAuthor(author);
		if (!file.isEmpty()) {
			String name = file.getOriginalFilename();
			event.setFileName(name);
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("/uploads", name)));
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		eventService.save(event);
		attributes.addFlashAttribute("message", "Evenement modifié avec succès!");

		return "redirect:eventLink?id=" + event.getIdevent();
	}

	@RequestMapping(path = "/eventLink", method = RequestMethod.GET)
	public String eventLink(Principal principal, @RequestParam("id") long idEvent, Model model) {

		Author author = eventService.findAuthorByUserName(principal.getName());
		if (!eventService.checkIfEventCorrespondToAuthor(idEvent, author)) {
			throw new AccessDeniedException("non authorisé");
		}

		Event event = eventService.findEvent(idEvent);
		List<Subscriber> subscribers = eventService.findByEvent(event);
		model.addAttribute("subscribers", subscribers);
		model.addAttribute("event", event);
		return "eventLink";
	}

	@RequestMapping(path = "/eventLink", method = RequestMethod.POST)
	public String eventLink(@RequestParam("idevent") Long idevent, @RequestParam("emailList") String emailList) {
		Event event = eventService.findEvent(idevent);
		eventService.addSubscribersToEvent(event, emailList);
		return "redirect:eventLink?id=" + event.getIdevent();
	}

	@RequestMapping(path = "/eventStat", method = RequestMethod.GET)
	public String eventStats(Principal principal, @RequestParam("id") long idEvent, Model model) {
		Author author = eventService.findAuthorByUserName(principal.getName());
		if (!eventService.checkIfEventCorrespondToAuthor(idEvent, author)) {
			throw new AccessDeniedException("non authorisé");
		}
		EventStatData eventStatData = new EventStatData();
		eventStatData.setIdEvent(idEvent);

		// recherche de l'evenement
		Event event = eventService.findEvent(idEvent);

		// recherche des membres ajoutés et statuts
		List<Subscriber> subscribers = eventService.findByEvent(event);
		eventStatData.setNbTotalInvitation(subscribers.size());

		// recherche du nombre de participants ACCEPT
		eventStatData.setNbIsPresent(eventService.countSubscriberByEventAndIsPresent(event, ACCEPT));

		// recherche du nombre de participants DECLINE
		eventStatData.setNbIsNotPresent(eventService.countSubscriberByEventAndIsPresent(event, DECLINE));

		// recherche du nombre de mails lus
		eventStatData.setNbIsRead(eventService.countSubscriberByEventAndMailStatus(event, 1));

		// recherche du nombre de presents apres ouverture du mail
		eventStatData.setNbIsPresentAfterReading(
				eventService.countSubscriberByEventAndIsPresentAndMailStatus(event, PRESENT, 1));

		// recherche du nombre d'absents apres ouverture du mail
		eventStatData.setNbIsAbsentAfterReading(
				eventService.countSubscriberByEventAndIsPresentAndMailStatus(event, NOTPRESENT, 1));

		model.addAttribute("subscribers", subscribers);
		model.addAttribute("eventStatData", eventStatData);
		model.addAttribute("event", event);

		return "eventStat";
	}

	@RequestMapping(value = "/image/{image_id:.*}")
	public void getImage(@PathVariable("image_id") String imageId, HttpServletResponse response) throws IOException {

		response.setHeader("Content-Type", MediaType.IMAGE_PNG_VALUE);
		Files.copy(Paths.get("C:/uploads/" + imageId), response.getOutputStream());

	}

}
