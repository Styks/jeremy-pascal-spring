package com.aiconoa.trainings.spring.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aiconoa.trainings.spring.domain.entity.Event;
import com.aiconoa.trainings.spring.domain.entity.Subscriber;
import com.aiconoa.trainings.spring.domain.entity.User;

public interface SubscriberRepository extends JpaRepository<Subscriber, Long>{

	List<Subscriber> findByEvent(Event event);

	Subscriber findByEventAndUser(Event event, User user);

	int countSubscriberByEventAndIsPresent(Event event, int accept);

	int countSubscriberByEventAndMailStatus(Event event, int mailStatus);

	int countSubscriberByEventAndIsPresentAndMailStatus(Event event, int present, int mailStatus);
	
}
