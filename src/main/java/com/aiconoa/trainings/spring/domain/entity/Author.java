package com.aiconoa.trainings.spring.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "author")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long idauthor;
    private String username;
    private String password;
    @Transient
    private String password2;
    @Column(nullable = true)
    private String token;

    public Author() {
        super();
    }

    public Author(int idauthor, String username, String password) {
        super();
        this.idauthor = idauthor;
        this.username = username;
        this.password = password;
    }

	public Author(int idauthor) {
        this.idauthor = idauthor;
    }

	public long getIdauthor() {
        return idauthor;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setIdauthor(int idauthor) {
        this.idauthor = idauthor;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}
}
