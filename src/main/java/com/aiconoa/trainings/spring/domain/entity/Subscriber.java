package com.aiconoa.trainings.spring.domain.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "subscribers")
public class Subscriber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name="hashcode")
    private String hashcode;
    @Column(name="mailstatus")
    private int mailStatus;
    @Transient
    private String email;
    @Column(name="ispresent")
    private int isPresent;
    @Transient
    private String isPresentString;
    @Column(name="sendingdate", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendingDate;
    @Transient
    private String sendingDateString;
    @Transient
    private String iconeLink;
    @Transient
    private static final int ACCEPT = 1;
    @Transient
    private static final int DECLINE = 2;

    @ManyToOne
    @JoinColumn(name = "iduser")
    private User user;

    @ManyToOne
    @JoinColumn(name = "idevent")
    private Event event;

    public Subscriber() {
        super();
    }

    public Subscriber(long idEvent, long iduser) {
        super();
        this.event = new Event(idEvent);
        this.user = new User(iduser);
    }

    public Subscriber(long idEvent, long iduser, int isPresent) {
        this(idEvent, iduser);
        this.isPresent = isPresent;

        checkIsPresent();
    }

    public Subscriber(String email, int isPresent, Date sendingDate) {
        super();
        this.email = email;
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        this.sendingDateString = formater.format(sendingDate);
        this.isPresent = isPresent;

        checkIsPresent();
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getIconeLink() {
        return iconeLink;
    }

    public String getEmail() {
        return user.getEmail();
    }

    public String getSendingDateString() {
        return sendingDateString;
    }

    public int getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(int isPresent) {
        this.isPresent = isPresent;
    }

    public String getIsPresentString() {
        return isPresentString;
    }

    public long getIdevent() {
        return event.getIdevent();
    }

    public void setIdEvent(long idevent) {
        this.event = new Event(idevent);
    }

    public void setIduser(long iduser) {
        this.user = new User(iduser);
    }

    public long getIduser() {
        return user.getIduser();
    }

    public String getHashcode() {
        return hashcode;
    }

    public int getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(int mailStatus) {
        this.mailStatus = mailStatus;
    }

    public Date getSendingDate() {
        return sendingDate;
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIsPresentString(String isPresentString) {
        this.isPresentString = isPresentString;
    }

    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    public void setSendingDateString(String sendingDateString) {
        this.sendingDateString = sendingDateString;
    }

    public void setIconeLink(String iconeLink) {
        this.iconeLink = iconeLink;
    }

    @PostLoad
    private void postLoad() {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        sendingDateString = formater.format(sendingDate);
        checkIsPresent();
    }

    private void checkIsPresent() {
        switch (isPresent) {
        case ACCEPT:
            this.isPresentString = "Présent";
            this.iconeLink = "/images/icone_valider.png";
            break;
        case DECLINE:
            this.isPresentString = "Absent";
            this.iconeLink = "/images/icon_delete.png";
            break;
        default:
            this.isPresentString = "Non confirmé";
            this.iconeLink = "/images/icone_interrogation.png";
            return;
        }
    }
}
