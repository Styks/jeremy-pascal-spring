package com.aiconoa.trainings.spring.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long idevent;
    private String title;
    private String description;
    private static final String LINKTOEVENTLINKSERVLET = "eventLink?id=";

    @Transient
    private String linkToEventLinkServlet;

    @Column(name="filename")
    private String fileName;

    @ManyToOne
    @JoinColumn(name = "idauthor")
    private Author author;

    public Event() {
        super();
    }

    public Event(String title, String description, long idEvent) {
        super();
        this.title = title;
        this.description = description;
        this.linkToEventLinkServlet = LINKTOEVENTLINKSERVLET + idEvent;
        this.setIdevent(idEvent);
    }

    public Event(String title, String description, long idEvent, String fileName) {
        super();
        this.title = title;
        this.description = description;
        this.idevent = idEvent;
        this.fileName = fileName;
        this.linkToEventLinkServlet = LINKTOEVENTLINKSERVLET + idEvent;
    }

    public Event(long idevent) {
        this.idevent = idevent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLinkToEventLinkServlet() {
        return linkToEventLinkServlet;
    }

    public long getIdevent() {
        return idevent;
    }

    public void setIdevent(long idEvent) {
        this.idevent = idEvent;
    }

    public Author getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setIdAuthor(int idAuthor) {
        this.author = new Author(idAuthor);
    }

    public void setLinkToEventLinkServlet(String linkToEventLinkServlet) {
        this.linkToEventLinkServlet = linkToEventLinkServlet;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @PostLoad
    private void setlinkToEventLinkServlet() {
        this.linkToEventLinkServlet = LINKTOEVENTLINKSERVLET + this.idevent;
    }
}
