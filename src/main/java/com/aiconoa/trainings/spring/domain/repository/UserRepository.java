package com.aiconoa.trainings.spring.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aiconoa.trainings.spring.domain.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

	User findByEmail(String email);
}
