package com.aiconoa.trainings.spring.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aiconoa.trainings.spring.domain.entity.Author;

public interface AuthorRepository extends JpaRepository<Author, Long>{
	Long countByUsername(String username);
	Author findByUsername(String username);
}
