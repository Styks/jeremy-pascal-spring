package com.aiconoa.trainings.spring.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aiconoa.trainings.spring.domain.entity.Author;
import com.aiconoa.trainings.spring.domain.entity.Event;

public interface EventRepository extends JpaRepository<Event, Long>{

	List<Event> findByAuthor(Author author);

	Event findEventByAuthor(Author author);

	long countByIdevent(long idEvent);
}
