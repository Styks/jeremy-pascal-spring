package com.aiconoa.trainings.spring.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
	private long iduser;
	private String email;
	
	public User() {
        super();
    }

    public User(long iduser, String email) {
		super();
		this.iduser = iduser;
		this.email = email;
	}
	
	public User(long iduser) {
        this.iduser = iduser;
    }

    public long getIduser() {
		return iduser;
	}
	
	public String getEmail() {
		return email;
	}

	public void setIduser(long iduser) {
		this.iduser = iduser;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
